FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

ARG JAVA_VERSION=1.8.0
ENV JAVA_VERSION=${JAVA_VERSION}
    

# Install deps
RUN INSTALL_PKGS="tar unzip bc which lsof java-${JAVA_VERSION}-openjdk java-${JAVA_VERSION}-openjdk-devel" && \
    yum update -y && \
    yum install -y --enablerepo=centosplus $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y
    
ENV JAVA_HOME=/etc/alternatives/java_sdk
RUN keytool -import -noprompt -trustcacerts -alias "CERN Root Certification Authority 2" -file "/etc/pki/tls/certs/CERN_Root_Certification_Authority_2.crt" -keystore "$JAVA_HOME/jre/lib/security/cacerts" --storepass changeit && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority" -file "/etc/pki/tls/certs/CERN_Certification_Authority.crt" -keystore "$JAVA_HOME/jre/lib/security/cacerts" --storepass changeit && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority(1)" -file "/etc/pki/tls/certs/CERN_Certification_Authority(1).crt" -keystore "$JAVA_HOME/jre/lib/security/cacerts" --storepass changeit

ENV HOME=${HOME:-/opt/app-root/src}
WORKDIR $HOME

CMD ["java", "-version"]